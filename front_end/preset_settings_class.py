import tkinter as tk


from back_end.model.config import config
from back_end.model.config.settings import Settings



class PresetsSettings:
    def __init__(self, root, header, entry_list, unit_list, drop_unit=None):
        self.header = tk.Label(root, text=header)
        self.entry_labels = []
        self.entry_list = []
        self.unit_list = list(unit_list)
        if drop_unit:
            self.drop_current = tk.StringVar()
            self.drop_current.set(drop_unit[0])

        for n in entry_list:
            # generate input labels
            self.entry_labels.append(tk.Label(root, text=n + ":"))
            # generate Units/Optionmenu for all selectable units
            # print(self.unit_list[entry_list.index(n)], entry_list.index(n), print(n))
            if self.unit_list[entry_list.index(n)] != "drop" and self.unit_list[entry_list.index(n)]:
                self.unit_list[entry_list.index(n)] = tk.Label(root, text=self.unit_list[entry_list.index(n)])
            elif self.unit_list[entry_list.index(n)] == "drop":
                self.unit_list[entry_list.index(n)] = tk.OptionMenu(root, self.drop_current, *drop_unit)
            # generate input fields
            if n == "Description":
                self.entry_list.append(tk.Text(root,
                                               exportselection=False,
                                               highlightcolor="black",
                                               width=10,
                                               height=20,
                                               xscrollcommand="scrollbar"
                                               ))
            else:
                self.entry_list.append(tk.Entry(root,
                                                exportselection=False,
                                                highlightcolor="black",
                                                justify="right",
                                                width=10,
                                                xscrollcommand="scrollbar"
                                                ))

    def place_preset_settings(self, relx, rely, relheight, relwidth, anchor, special=False):
        #Automatically defines position of preset button on the settings page according to amount of already present presets.  
        self.header.place(relx=relx, rely=rely, relheight=relheight, relwidth=relwidth, anchor=anchor)
        for n in range(len(self.entry_list)):
            if special == True and n == len(self.entry_list) - 1:
                self.entry_labels[n].place(relx=relx - relwidth * 0.8, rely=rely + relheight * 0.02 / 0.05 * (n + 3),
                                           relheight=relheight * 0.02 / 0.05 * 3, relwidth=relwidth * 0.05 / 0.25,
                                           anchor=anchor)
                self.entry_labels[n].configure(anchor="n")
            else:
                self.entry_labels[n].place(relx=relx - relwidth * 0.8, rely=rely + relheight * 0.02 / 0.05 * (n + 1),
                                           relheight=relheight * 0.02 / 0.05, relwidth=relwidth * 0.05 / 0.25,
                                           anchor=anchor)
            if self.unit_list[n]:
                self.unit_list[n].place(relx=relx - relwidth * 0.0, rely=rely + relheight * 0.02 / 0.05 * (n + 1),
                                        relheight=relheight * 0.02 / 0.05, relwidth=relwidth * 0.04 / 0.25,
                                        anchor=anchor)

                self.entry_list[n].place(relx=relx - relwidth * 0.16, rely=rely + relheight * 0.02 / 0.05 * (n + 1),
                                         relheight=relheight * 0.02 / 0.05, relwidth=relwidth * 0.16 / 0.25,
                                         anchor=anchor)
            else:
                if special == True and n == len(self.entry_list)-1:
                    self.entry_list[n].place(relx=relx - relwidth * 0.0, rely=rely + relheight * 0.02 / 0.05 * (n + 3),
                                             relheight=relheight * 0.06 / 0.05, relwidth=relwidth * 0.20 / 0.25,
                                             anchor=anchor)
                else:
                    self.entry_list[n].place(relx=relx - relwidth * 0.0, rely=rely + relheight * 0.02 / 0.05 * (n + 1),
                                             relheight=relheight * 0.02 / 0.05, relwidth=relwidth * 0.20 / 0.25,
                                             anchor=anchor)

    def update_entries(self, update_list, last_is_description=False):
        #stores modified entries into currently selected preset
        def set_entry(entry_object, value, special=False):
            if special:
                entry_object.delete('1.0', 'end')
            else:
                entry_object.delete(0, 'end')
            entry_object.insert('end', value)
        for n in range(len(update_list)):
            if n == len(update_list)-1 and last_is_description:
                set_entry(self.entry_list[n], update_list[n], special=True)
            else:
                set_entry(self.entry_list[n], update_list[n])

    def delete(self):
        #Completely deletes preset from saved presets list. is called by remove button on the settings page
        self.header.destroy()
        for n in range(len(self.entry_list)):
            # print(n)
            self.entry_labels[n].destroy()
            if self.unit_list[n]:
                self.unit_list[n].destroy()
                self.entry_list[n].destroy()
            else:
                self.entry_list[n].destroy()


def update_objects_on_settings_page(user, freq, azimuth, elevation, settings_object):
    #refreshes settings page object
    user.update_entries([settings_object.user_parameters.operator,
                         settings_object.user_parameters.company_name,
                         settings_object.user_parameters.project_name,
                         settings_object.user_parameters.antenna_name,
                         settings_object.user_parameters.description],
                        last_is_description=True)

    freq.update_entries([settings_object.current_preset.start_frequency,
                         settings_object.current_preset.stop_frequency,
                         settings_object.current_preset.steps_in_frequency_test_band,
                         settings_object.current_preset.power_level_dBm,
                         settings_object.current_preset.bandwidth_kHz])

    azimuth.update_entries([settings_object.current_preset.start_az,
                            settings_object.current_preset.stop_az,
                            settings_object.current_preset.az_steps])

    elevation.update_entries([settings_object.current_preset.start_el,
                              settings_object.current_preset.stop_el,
                              settings_object.current_preset.el_steps])


def update_dataclass_using_settings(user, freq, azimuth, elevation, settings_object):
    settings_object.user_parameters.operator = check_string(user.entry_list[0].get())
    settings_object.user_parameters.company_name = check_string(user.entry_list[1].get())
    settings_object.user_parameters.project_name = check_string(user.entry_list[2].get())
    settings_object.user_parameters.antenna_name = check_string(user.entry_list[3].get())
    settings_object.user_parameters.description = check_string(user.entry_list[4].get(1.0, 'end').strip())
    settings_object.current_preset.start_el = float(elevation.entry_list[0].get())
    settings_object.current_preset.stop_el = float(elevation.entry_list[1].get())
    settings_object.current_preset.el_steps = int(elevation.entry_list[2].get())
    settings_object.current_preset.start_az = float(azimuth.entry_list[0].get())
    settings_object.current_preset.stop_az = float(azimuth.entry_list[1].get())
    settings_object.current_preset.az_steps = int(azimuth.entry_list[2].get())
    settings_object.current_preset.start_frequency = int(freq.entry_list[0].get())
    settings_object.current_preset.stop_frequency = int(freq.entry_list[1].get())
    settings_object.current_preset.steps_in_frequency_test_band = int(freq.entry_list[2].get())
    settings_object.current_preset.power_level_dBm = int(freq.entry_list[3].get())
    settings_object.current_preset.bandwidth_kHz = float(freq.entry_list[4].get())


def update_objects_on_config_page(vnas, pos, settings_object):
    vna_keys = list(config.get_vnas())
    for n in range(len(vnas)):
        ports_string = ""
        for port in settings_object.vnas[vna_keys[n]].ports:
            ports_string = ports_string + str(port) + ", "
        ports_string = ports_string.strip(", ")
        vnas[n][0].update_entries([settings_object.vnas[vna_keys[n]].name,
                                   settings_object.vnas[vna_keys[n]].ip_address,
                                   settings_object.vnas[vna_keys[n]].tx_port,
                                   ports_string])

    pos.update_entries([settings_object.positioner.ip,
                        settings_object.positioner.port,
                        settings_object.positioner.ph,
                        settings_object.positioner.pv,
                        settings_object.positioner.offset_el,
                        settings_object.positioner.offset_az])


def update_dataclass_using_config(vnas, pos, settings_object):
    vna_keys = list(config.get_vnas())
    for n in range(len(vnas)):
        settings_object.vnas[vna_keys[n]].name = check_string(vnas[n][0].entry_list[0].get())
        settings_object.vnas[vna_keys[n]].ip_address = check_string(vnas[n][0].entry_list[1].get())
        settings_object.vnas[vna_keys[n]].tx_port = check_string(vnas[n][0].entry_list[2].get())
        settings_object.vnas[vna_keys[n]].ports = list(check_string(vnas[n][0].entry_list[3].get()).split(", "))

    settings_object.positioner.ip = check_string(pos.entry_list[0].get())
    settings_object.positioner.port = int(pos.entry_list[1].get())
    settings_object.positioner.ph = int(pos.entry_list[2].get())
    settings_object.positioner.pv = int(pos.entry_list[3].get())
    settings_object.positioner.offset_el = int(pos.entry_list[4].get())
    settings_object.positioner.offset_az = int(pos.entry_list[5].get())


def check_string(string):
    #Could be enabled for checking string inputs, however this check might cause errors
    #  if re.search("^[A-Za-z0-9.,\s]{1,}[\.]{0,1}[A-Za-z\s]{0,}$", string) is None:
    #        raise ValueError("Input may only consist of letters, numbers and spaces")
    return string


def main():
    #Example code for Class not used in main qui

    settings = Settings()  # settings object should be shared across entire application
    config.init(settings)  # load settings object with values from config, creates config if it doesn't yet exist

    window = tk.Tk()  # root addressing in tk, window = root
    window.geometry("1920x1080")
    window.configure(bg='white')
    window.title("ASTRON Antenna Testing Facility")

    user = PresetsSettings(window, "User Parameters",
                           ["Operator", "Company", "Project Name", "Antenna name", "Description"],
                           [False, False, False, False, False])
    
    freq = PresetsSettings(window, "Frequency Settings",
                           ["Frequency start", "Frequency stop", "Frequency step", "Power level", "Bandwidth"],
                           ['MHz', 'MHz', "Int", "DBm", "KHz"],
                           ["Int", "GHz", "MHz", "KHz", "Hz"])

    az = PresetsSettings(window, "Azimuth Settings",
                         ["Azimuth start", "Azimuth stop", "Azimuth step"],
                         ["degrees", "degrees", "Int"],
                         ["Degrees", "Int"])

    el = PresetsSettings(window, "Elevation Settings",
                         ["Elevation start", "Elevation stop", "Elevation step"],
                         ["degrees", "degrees", "Int"],
                         ["Degrees", "Int"])

    user.place_preset_settings(0.30, 0.15, 0.05, 0.25, "se", special=True)
    az.place_preset_settings(0.30, 0.38, 0.05, 0.25, "se")
    el.place_preset_settings(0.30, 0.54, 0.05, 0.25, "se")
    freq.place_preset_settings(0.30, 0.70, 0.05, 0.25, "se")

    update_objects_on_settings_page(user, freq, az, el, settings)

    window.mainloop()


if __name__ == '__main__':
    main()
