# tkinter libraries, for reference use: https://docs.python.org/3/library/tkinter.html

from tkinter.filedialog import askdirectory

from back_end.model.plot import plot
from back_end.model.config import config
from back_end.model.config.settings import Settings
from front_end.preset_settings_class import PresetsSettings, update_objects_on_settings_page, \
    update_dataclass_using_settings, \
    update_objects_on_config_page, update_dataclass_using_config
from front_end.configpage_setup import generate_vna_widgets, generate_vna
from front_end.measurement_page_setup import generate_preset, remove_preset, generate_preset_selection_removal_widgets, \
    gui_switch_preset
import back_end.model.measurement as M
from front_end.TKConsole import console_init, enable_disable_console, toggle_autoscroll

import tkinter as tk
from tkinter import *
from tkinter.ttk import Progressbar
from tkinter import messagebox
from tkinter import simpledialog

# matplotlib for Surface plot
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import random
import numpy as np

global settings  # Settings object
global add_vna_button, add_preset_button, el, az, freq, user, scrollbar, widget_text, console, canvas, ax, fig  # GUI elements
global current_setting_field, freq_slider, bar, drop, measurement_settings_button, main_page_button  # GUI elements
global configuration_menu_button, start_measurement_button, export_button, stop_button, preset_list, old_stdout  # GUI elements
global vna_list, pos, root, stop_callback, autoscroll_button

drawcanvas = True
refresh = True
path = "exports"


def enable_buttons():
    # unlocks main page menu buttons after operation of test facility so settings can't be tampered when running
    global refresh
    configuration_menu_button['state'] = tk.NORMAL
    measurement_settings_button['state'] = tk.NORMAL
    start_measurement_button['state'] = tk.NORMAL
    export_button['state'] = tk.NORMAL
    refresh = False


def disable_buttons():
    # locks main page menu buttons during operation of test facility so settings can't be tampered when running
    global refresh
    configuration_menu_button['state'] = tk.DISABLED
    measurement_settings_button['state'] = tk.DISABLED
    start_measurement_button['state'] = tk.DISABLED
    export_button['state'] = tk.DISABLED
    refresh = True


def start_measurement(settings_controller):
    # calls measurement functions through the controller and starts the plotting of received measurement data on the surface plot.

    global draw_plot, selected_port, drop, list_ports, settings
    settings = settings_controller
    
    # Current settings field
    # Calculate Estimated Time
    if settings.current_preset.start_el > settings.current_preset.stop_el:
        difference = abs(settings.current_preset.start_el - settings.current_preset.stop_el)
    else:
        difference = abs(settings.current_preset.stop_el - settings.current_preset.start_el)
    time_estimate = difference * settings.current_preset.az_steps + (
                settings.current_preset.az_steps * settings.current_preset.el_steps * 0.6)
    current_setting_field['state'] = tk.NORMAL
    current_setting_field.delete('1.0', 'end')
    current_setting_field.insert('end',
                                 f"Current preset: {settings.current_preset.preset_name}    User: {settings.user_parameters.operator}\nFrequency: {settings.current_preset.start_frequency}MHz/{settings.current_preset.stop_frequency}MHz Steps: {settings.current_preset.steps_in_frequency_test_band}\nAzimuth:{settings.current_preset.start_az}/{settings.current_preset.stop_az} Steps: {settings.current_preset.az_steps}\nElevation: {settings.current_preset.start_el}/{settings.current_preset.stop_el} Steps: {settings.current_preset.el_steps}\nEstimated total time for measurement: {time_estimate/60:.2f} minutes")
    current_setting_field['state'] = tk.DISABLED
    
    
    
    draw_plot = True
    disable_buttons()

    step = int((settings.current_preset.stop_frequency - settings.current_preset.start_frequency) / (
            settings.current_preset.steps_in_frequency_test_band - 1))
    print(settings.current_preset.start_frequency)
    if settings.current_preset.start_frequency % step != 0:
        messagebox.showwarning(title="Warining!", message="Warning: start frequency is not multiple of frequency step size,\n this causes a bug in the frequency slider, no data is lost.")
    freq_slider.config(from_=settings.current_preset.start_frequency,
                       to=settings.current_preset.stop_frequency,
                       tickinterval=step, resolution=step)
    freq_slider['state'] = tk.NORMAL

    # init Plot Objects based on current paramiters
    list_az = np.arange(settings.current_preset.start_az,
                        settings.current_preset.stop_az + (
                                settings.current_preset.stop_az - settings.current_preset.start_az) / (settings.current_preset.az_steps-1),
                        (
                                settings.current_preset.stop_az - settings.current_preset.start_az) / (settings.current_preset.az_steps-1)).tolist()
    list_el = np.arange(settings.current_preset.start_el,
                        settings.current_preset.stop_el + (
                                settings.current_preset.stop_el - settings.current_preset.start_el) / (settings.current_preset.el_steps-1),
                        (
                                settings.current_preset.stop_el - settings.current_preset.start_el) / (settings.current_preset.el_steps-1)).tolist()
    print(list_el)
    print(list_az)
    list_ports = config.get_all_vna_port_names()
    selected_port.set(list_ports[0])
    drop.destroy()
    drop = OptionMenu(window, selected_port, *list_ports, command=surface_plot_gui)
    drop.place(relx=0.005,
               rely=0.015,
               anchor='nw')
    drop['state'] = tk.NORMAL
    list_frequencies = np.arange(settings.current_preset.start_frequency, settings.current_preset.stop_frequency + step,
                                 step)
    plot.init_plot_objects(list_az, list_el, list_ports, list_frequencies)
    surface_plot_gui()
    if demo_mode.get():
        update_objects_on_settings_page(user, freq, az, el, settings)
    test()


def switch_to_page():
    # Saves entered data first from page, then clears entire window in order to switch to widgets of other page again.
    global settings
    try:
        update_dataclass_using_settings(user, freq, az, el, settings)
        update_dataclass_using_config(vna_list, pos, settings)
        config.update_current_preset()
        config.update_config_file()
    except ValueError as error:
        messagebox.showerror('error', error)
        return False
    console.pack_forget()
    # clears all elements from UI window and then calls required page function
    for widget in window.winfo_children():
        widget.place_forget()
    window.update()
    return True


def surface_plot_gui(n=0):
    # Surface Plot generation function. Uses Matplotlib to create the visualisation of measurement data
    global fig, ax, canvas, bar
    selected_frequency_number = int((var.get() - settings.current_preset.start_frequency) / (
            (settings.current_preset.stop_frequency - settings.current_preset.start_frequency) / (
            settings.current_preset.steps_in_frequency_test_band - 1)))
    print("Updating 3D plot, currently selected plot number is:",
          selected_frequency_number + settings.current_preset.steps_in_frequency_test_band * list_ports.index(
              str(selected_port.get())))
    ax.cla()
    x, y, z, name = plot.object_list[
        selected_frequency_number + settings.current_preset.steps_in_frequency_test_band * list_ports.index(
            str(selected_port.get()))].draw_plot()

    # x = np.outer(np.linspace(-2, 2, 30), np.ones(30))
    # y = x.copy().T # transpose
    # z = np.cos(x ** 2 + y ** 2)

    ax.plot_surface(x, y, z, cmap='viridis', edgecolor='none')
    ax.set_title(name + 'MHz')
    ax.set_xlabel("Azimuth")
    ax.set_ylabel("Elevation")
    ax.set_zlabel("Gain")
    canvas.draw()
    
    #Update progress bar if refresh loop is active or progress is not 100 or 0
    if refresh or (int(bar['value']) != 100 or int(bar['value'] != 0)):
        progress = 0
        for i in z.tolist():
            progress = progress + i.count('nan')
        bar['value'] = int(round((settings.current_preset.az_steps*settings.current_preset.el_steps - progress)/(settings.current_preset.az_steps*settings.current_preset.el_steps)*100))

    #Lifts all widgets above canvas with surface plot
    stop_button.lift()
    export_button.lift()
    start_measurement_button.lift()
    bar.lift()
    configuration_menu_button.lift()
    measurement_settings_button.lift()
    drop.lift()
    current_setting_field.lift()


def generate_png(filepath=''):
    global fig, ax, canvas, drawcanvas, refresh
    drop['state'] = tk.DISABLED
    freq_slider['state'] = tk.DISABLED
    refresh = False
    # canvas.get_tk_widget().place_forget()
    m = 0
    for n in plot.object_list:
        x, y, z, name = n.draw_plot()
        ax.cla()
        surf = ax.plot_surface(x, y, z, cmap='viridis', edgecolor='none')
        surf._facecolors2d = surf._facecolor3d
        surf._edgecolors2d = surf._edgecolor3d
        ax.set_title(name + 'MHz')
        ax.set_xlabel("Azimuth")
        ax.set_ylabel("Elevation")
        ax.set_zlabel("Gain")
        canvas.draw()
        #print(int(np.log(len(plot.object_list))), np.log10(len(plot.object_list)), len(plot.object_list))
        filename = filepath + '\Surface' + str(m).zfill(int(np.log10(len(plot.object_list))+1)) + name + 'MHz'
        print("Saving PNG:", filename)
        fig.savefig(filename, transparent=True, bbox_inches='tight', dpi=300, edgecolor=None)
        m += 1
    drop['state'] = tk.NORMAL
    freq_slider['state'] = tk.NORMAL
    # drawcanvas = True
    surface_plot_gui()


def test():
    surface_plot_gui()
    if refresh:
        window.after(1000, test)


# general configuration
def exit_window():
    # Exit_window kills thread before destroying window. this prevents unwanted behaviour
    def destroy_window():
        window.quit()
        window.destroy()

    stop_callback()
    print("\nClosing threads...")
    enable_disable_console(False)
    if random.random() < 0.1:
        print("I don't hate you")
    window.after(2000, destroy_window)


window = tk.Tk()  # root addressing in tk, window = root
window.protocol("WM_DELETE_WINDOW", exit_window)
var = DoubleVar()
selected_port = StringVar()
selected_port.set("None")
demo_mode = tk.BooleanVar()
demo_mode.set(False)
window.geometry("1920x1080")
window.configure(bg='white')
window.title("ASTRON Antenna Testing Facility")
window.iconbitmap(default=r'media\favicon.ico')
window.option_add("*font", "Segoe 10")  # changes default font to something more readable
window.state('zoomed')


def main_page():
    checktype = switch_to_page()
    if not checktype:
        return
    drop.place(relx=0.005,
               rely=0.015,
               anchor='nw')
    console.place(relx=1,
                  rely=0.85,
                  relheight=0.85,
                  relwidth=0.40,
                  anchor='se')
    global canvas, drawcanvas
    if drawcanvas:
        canvas.get_tk_widget().place(relx=0.35,
                                     rely=0.45,
                                     relheight=1,
                                     relwidth=0.9 / (16 / 9),
                                     anchor='center')
    # Frequency slider selector
    freq_slider.place(relx=0.005, rely=0.20, height=500)

    # Export function, progress bar and button
    export_button.place(relx=0.3,
                        rely=1,
                        relheight=0.1,
                        relwidth=0.1,
                        anchor='se')

    # Progress bar
    bar.place(relx=1,
              rely=0.87,
              relwidth=0.4,
              relheight=0.02,
              anchor='se')

    # Start measurement Button
    start_measurement_button.place(relx=0.7,
                                   rely=1,
                                   relheight=0.1,
                                   relwidth=0.1,
                                   anchor='se')

    # EMERGENCY STOP BUTTON
    stop_button.place(relx=0.8,
                      rely=1,
                      relheight=0.1,
                      relwidth=0.1,
                      anchor='se')

    # Configuration Menu Button
    configuration_menu_button.place(relx=0.2,
                                    rely=1,
                                    relheight=0.1,
                                    relwidth=0.1,
                                    anchor='se')

    # Measurement Settings Button
    measurement_settings_button.place(relx=0.1,
                                      rely=1,
                                      relheight=0.1,
                                      relwidth=0.1,
                                      anchor='se')

    # Current settings field
    # Calculate Estimated Time
    if settings.current_preset.start_el > settings.current_preset.stop_el:
        difference = abs(settings.current_preset.start_el - settings.current_preset.stop_el)
    else:
        difference = abs(settings.current_preset.stop_el - settings.current_preset.start_el)
    time_estimate = difference * settings.current_preset.az_steps + (
                settings.current_preset.az_steps * settings.current_preset.el_steps * 0.6)

    current_setting_field['state'] = tk.NORMAL
    current_setting_field.delete('1.0', 'end')
    current_setting_field.insert('end',
                                 f"Current preset: {settings.current_preset.preset_name}    User: {settings.user_parameters.operator}\nFrequency: {settings.current_preset.start_frequency}MHz/{settings.current_preset.stop_frequency}MHz Steps: {settings.current_preset.steps_in_frequency_test_band}\nAzimuth:{settings.current_preset.start_az}/{settings.current_preset.stop_az} Steps: {settings.current_preset.az_steps}\nElevation: {settings.current_preset.start_el}/{settings.current_preset.stop_el} Steps: {settings.current_preset.el_steps}\nEstimated total time for measurement: {time_estimate/60:.2f} minutes")
    current_setting_field.place(relx=1,
                                rely=1,
                                relheight=0.1,
                                relwidth=0.2,
                                anchor='se')
    current_setting_field.lift()
    current_setting_field['state'] = tk.DISABLED

    stop_button.lift()
    export_button.lift()
    start_measurement_button.lift()
    bar.lift()
    configuration_menu_button.lift()
    measurement_settings_button.lift()
    drop.lift()
    current_setting_field.lift()


def configuration_page():
    checktype = switch_to_page()
    if not checktype:
        return
    # Main page Button
    main_page_button.place(relx=0.1,
                           rely=1,
                           relheight=0.1,
                           relwidth=0.1,
                           anchor='se')

    measurement_settings_button.place(relx=0.2,
                                      rely=1,
                                      relheight=0.1,
                                      relwidth=0.1,
                                      anchor='se')

    tk.Label(window, text='Vector Network Analyser Settings').place(relx=0.45,
                                                                    rely=0.20,
                                                                    relheight=0.05,
                                                                    relwidth=0.25,
                                                                    anchor='se')
    pos.place_preset_settings(0.80, 0.20, 0.05, 0.25, "se")
    generate_vna_widgets(vna_list, window, pos, settings)
    add_vna_button.place(relx=0.45 + 0.05 * 9 / 16,
                         rely=0.20,
                         relheight=0.05,
                         relwidth=0.05 * 9 / 16,
                         anchor='se')


def measurement_settings_page():
    checktype = switch_to_page()
    if not checktype:
        return
    # Main page Button
    main_page_button.place(relx=0.1,
                           rely=1,
                           relheight=0.1,
                           relwidth=0.1,
                           anchor='se')

    # Configuration menu Button
    configuration_menu_button.place(relx=0.2,
                                    rely=1,
                                    relheight=0.1,
                                    relwidth=0.1,
                                    anchor='se')

    # All entry fields
    user.place_preset_settings(0.30, 0.15, 0.05, 0.25, "se", special=True)
    az.place_preset_settings(0.30, 0.38, 0.05, 0.25, "se")
    el.place_preset_settings(0.30, 0.54, 0.05, 0.25, "se")
    freq.place_preset_settings(0.30, 0.70, 0.05, 0.25, "se")

    # Presets table
    tk.Label(window, text='Measurement Settings Preset').place(relx=0.8,
                                                               rely=0.10,
                                                               relheight=0.05,
                                                               relwidth=0.2,
                                                               anchor='se')
    generate_preset_selection_removal_widgets(preset_list, window, settings, user, freq, az, el)
    add_preset_button.place(relx=0.8 + 0.05 * 9 / 16,
                            rely=0.10,
                            relheight=0.05,
                            relwidth=0.05 * 9 / 16,
                            anchor='se')


def init_gui_elements(start_measurement_callback, stop_measurement_callback, generate_pdf_callback):
    global stop_button, export_button, start_measurement_button, configuration_menu_button, main_page_button
    global measurement_settings_button, drop, bar, freq_slider, current_setting_field, fig, ax, canvas, console
    global widget_text, scrollbar, old_stdout, preset_list, user, freq, az, el, add_preset_button, add_vna_button
    global vna_list, pos
    global selected_port
    global stop_callback
    global autoscroll_button

    stop_callback = stop_measurement_callback
    # Buttons
    preset_list = []
    stop_button = tk.Button(window, text="STOP", height=5, width=20, bg="red", command=stop_callback)
    export_button = tk.Button(window, text="Export", height=5, width=20, command=lambda: generate_pdf_callback(path),
                              state=tk.DISABLED)
    start_measurement_button = tk.Button(window, text="Start Measurement", height=5, width=20,
                                         command=lambda demo_mode=demo_mode: start_measurement_callback(demo_mode.get(),
                                                                                                        path))
    configuration_menu_button = tk.Button(window, text="Configuration Menu", height=5, width=20,
                                          command=configuration_page)
    main_page_button = tk.Button(window, text="Main Page", height=5, width=20, command=main_page)
    measurement_settings_button = tk.Button(window, text="Measurement Settings", height=5, width=20,
                                            command=measurement_settings_page)

    # Miscellaneous UI elements
    drop = OptionMenu(window, selected_port, *["None"], command=surface_plot_gui)
    drop['state'] = tk.DISABLED
    bar = Progressbar(window, orient='horizontal')
    bar['value'] = 0
    freq_slider = Scale(window, from_=0, to=0, tickinterval=0, resolution=0, orient=VERTICAL,
                        command=surface_plot_gui, bg="white", variable=var)
    freq_slider['state'] = tk.DISABLED
    current_setting_field = tk.Text(window, exportselection=False, highlightcolor="black", bg="whitesmoke", width=5,
                                    height=40)

    # Surface plot stuff
    fig = plt.figure(figsize=(12, 12), clear=True, num=1)
    ax = plt.axes(projection='3d')
    canvas = FigureCanvasTkAgg(fig, master=window)

    # console output frame
    console = console_init(window)

    # Menubar
    menubar = Menu(window)
    window.config(menu=menubar)
    # Define Submenu
    file_menu = Menu(menubar, tearoff=0)
    console_menu = Menu(menubar, tearoff=0)
    extras_menu = Menu(menubar, tearoff=0)

    # Add Submenu to Main menubar
    menubar.add_cascade(label="File", menu=file_menu)
    menubar.add_cascade(label="Console", menu=console_menu)
    menubar.add_cascade(label="Extras", menu=extras_menu)

    # Submenu functions
    def get_path():
        global path
        path = askdirectory()
        print("Export path:", path)

    file_menu.add_command(label="Export Root", command=lambda: get_path())
    console_menu.add_command(label="Autoscroll", command=toggle_autoscroll)
    console_menu.add_separator()
    console_menu.add_command(label="Enable/Disable", command=enable_disable_console)
    extras_menu.add_checkbutton(label="Demo mode", onvalue=1, offvalue=0, variable=demo_mode)
    console_menu.entryconfigure("Enable/Disable", state="disabled")

    # init entry fields for config pages
    user = PresetsSettings(window, "User Parameters",
                           ["Operator", "Company", "Project Name", "Antenna name", "Description"],
                           [False, False, False, False, False])

    freq = PresetsSettings(window, "Frequency Settings",
                           ["Frequency start", "Frequency stop", "Frequency step", "Power level", "Bandwidth"],
                           ['MHz', 'MHz', "Int", "DBm", "KHz"],
                           ["Int", "GHz", "MHz", "KHz", "Hz"])

    az = PresetsSettings(window, "Azimuth Settings",
                         ["Azimuth start", "Azimuth stop", "Azimuth step"],
                         ["degrees", "degrees", "Int"],
                         ["Degrees", "Int"])

    el = PresetsSettings(window, "Elevation Settings",
                         ["Elevation start", "Elevation stop", "Elevation step"],
                         ["degrees", "degrees", "Int"],
                         ["Degrees", "Int"])

    update_objects_on_settings_page(user, freq, az, el, settings)
    add_preset_button = tk.Button(window, text='+',
                                  command=lambda preset_list=preset_list, root=window, settings=settings, user=user,
                                                 freq=freq, az=az, el=el: generate_preset(preset_list, root, settings,
                                                                                          user, freq, az, el))

    vna_list = []
    pos = PresetsSettings(window, "Positioner Settings",
                          ["IP address", "PH", "PV", "TCP Port", "Azimuth offset", "Elevation offset"],
                          [False, False, False, False, False, False])
    add_vna_button = tk.Button(window, text='+', command=
    lambda vna_list=vna_list, root=window, pos=pos, settings=settings: generate_vna(vna_list, root, pos,
                                                                                    settings))

    update_objects_on_config_page(vna_list, pos, settings)


def main(glob_settings, start_measurement_callback, stop_measurement_callback, generate_pdf_callback):
    # when starting the application for the first time and the window.mainloop() function of Tkinter which loads the GUI.
    global settings
    settings = glob_settings

    init_gui_elements(start_measurement_callback, stop_measurement_callback, generate_pdf_callback)
    # call main_page() for initial start
    main_page()
    # print(list(settings.vnas["k"].ports.split(" ")))
    # Main window root function. do NOT remove as no GUI will load if not present.
    # window.protocol("WM_DELETE_WINDOW", window.iconify) (additional optional feature)
    window.mainloop()
