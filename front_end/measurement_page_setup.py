import tkinter as tk
from tkinter import simpledialog, messagebox

from front_end.preset_settings_class import update_objects_on_settings_page, update_dataclass_using_settings
from back_end.model.config import config


def gui_switch_preset(preset_name, indent_num, user, freq, az, el, settings, preset_list, remove=False):
    try:
        if remove == False:
            update_dataclass_using_settings(user, freq, az, el, settings)
            config.update_current_preset()
        config.switch_current_preset(preset_name)
        print('Current preset: ' + str(settings.current_preset))
        update_objects_on_settings_page(user, freq, az, el, settings)
    except ValueError as error:
        messagebox.showerror('error', error)
        return
    for n in range(len(preset_list)):
        # print(n, indent_num)
        if n == indent_num:
            preset_list[n][0]['state'] = tk.DISABLED
        else:
            preset_list[n][0]['state'] = tk.NORMAL


def remove_preset(preset_list, root, settings, user, freq, az, el, preset):
    if messagebox.askokcancel('Ok Cancel', 'Remove Preset: ' + preset + '?'):
        try:
            gui_switch_preset("default", 0, user, freq, az, el, settings, preset_list, remove=True)
            config.remove_preset(preset)
            generate_preset_selection_removal_widgets(preset_list, root, settings, user, freq, az, el)
        except ValueError as error:
            messagebox.showerror('error', error)


def generate_preset(preset_list, root, settings, user, freq, az, el, n=0):
    name = simpledialog.askstring("Name:", "New preset name?")
    if name is None:
        return
    try:
        config.add_preset(name, 5, 0.1, 2000, 3000, 31, -90, 90, 10, -100, 100, 10)
        generate_preset_selection_removal_widgets(preset_list, root, settings, user, freq, az, el)
    except ValueError as error:
        messagebox.showerror('error', f"Preset name: '{name}' {error}!")


def generate_preset_selection_removal_widgets(preset_list, root, settings, user, freq, az, el):
    for n in preset_list:
        for m in n:
            m.destroy()
    preset_list.clear()
    m = 0
    print(list(config.get_presets()))
    for n in list(config.get_presets()):
        preset_list.append([])
        preset_list[m].append(
            tk.Button(root, text=n, command=(
                lambda n=n, m=m, preset_list=preset_list, root=root, settings=settings, user=user, freq=freq, az=az,
                       el=el: gui_switch_preset(n, m, user, freq, az, el, settings, preset_list))))  # lambda :Gui
        preset_list[m][0].place(relx=0.8,
                                rely=0.10 + (m + 1) * 0.05,
                                relheight=0.05,
                                relwidth=0.2,
                                anchor='se')

        preset_list[m].append(tk.Button(root, text="X", command=(
            lambda n=n, preset_list=preset_list, root=root, settings=settings, user=user, freq=freq, az=az,
                   el=el: remove_preset(preset_list, root, settings, user, freq, az, el, n))))
        preset_list[m][1].place(relx=0.80 + 0.05 * 9 / 16,
                                rely=0.10 + (m + 1) * 0.05,
                                relheight=0.05,
                                relwidth=0.05 * 9 / 16,
                                anchor='se')
        m = m + 1
    preset_list[list(config.get_presets()).index(settings.current_preset.preset_name)][0]['state'] = tk.DISABLED
