from front_end.preset_settings_class import update_objects_on_config_page, PresetsSettings
import tkinter as tk
from tkinter import messagebox, simpledialog
from back_end.model.config import config


def generate_vna_widgets(vna_list, root, pos, settings):
    #Generates add and remove buttons for VNA settings
    for n in vna_list:
        #Remove old VNA widgets
        n[0].delete()
        del n[0]  # remove object
        n[0].destroy()  # remove button
    vna_list.clear()
    #Generates new VNA widgets and places them on gui 
    m = 0
    for n in list(config.get_vnas()):
        vna_list.append([])
        vna_list[m].append(PresetsSettings(root, n,
                                           ["Name", "IP address", "Tx port", "Ports"],
                                           [False, False, False, False]))
        vna_list[m][0].place_preset_settings(0.45, 0.25 + m * 0.131, 0.05, 0.25, "se")
        vna_list[m].append(tk.Button(root, text="X", command=(
            lambda vna=n, vna_list=vna_list, root=root, pos=pos, settings=settings: remove_vna(vna, vna_list, root, pos,
                                                                                               settings))))
        vna_list[m][1].place(relx=0.45 + 0.05 * 9 / 16,
                             rely=0.25 + m * 0.131,
                             relheight=0.05,
                             relwidth=0.05 * 9 / 16,
                             anchor='se')
        m = m + 1
    update_objects_on_config_page(vna_list, pos, settings)


def remove_vna(vna, vna_list, root, pos, settings):
    if messagebox.askokcancel('Ok Cancel', 'Remove VNA: ' + vna + '?'):
        try:
            config.remove_vna(vna)
            generate_vna_widgets(vna_list, root, pos, settings)
        except ValueError as error:
            messagebox.showerror('error', error)


def generate_vna(vna_list, root, pos, settings):
    name = simpledialog.askstring("Name:", "New VNA name?")
    if name is None:
        return
    try:
        config.add_vna(name, name, '1.2.3.4', 1, [1, 2, 3, 4])
        generate_vna_widgets(vna_list, root, pos, settings)
    except ValueError as error:
        messagebox.showerror('error', f"VNA name: '{name}' {error}!")
