import tkinter as tk
import sys

autoscroll = True
enabled = True

class Redirect:
    #Class is used to catch console output and input it into text widget
    def __init__(self, widget):
        self.widget = widget

    def write(self, text):
        self.widget.insert('end', text)
        if autoscroll:
            self.widget.see("end")  # autoscroll
    
def toggle_autoscroll():
    global autoscroll
    autoscroll = not autoscroll

def enable_disable_console(Toggle = True):
    global old_stdout, widget_text, enabled
    if Toggle == True: #make sure that console is disabled rather than toggle it
        enabled = not enabled
        if enabled:
            old_stdout = sys.stdout
            sys.stdout = Redirect(widget_text)
        else:
            sys.stdout = old_stdout
    else:
        sys.stdout = old_stdout 

def console_init(root):
    global old_stdout, widget_text
    console = tk.Frame(root, bg="black")

    widget_text = tk.Text(console, bg="#282c34", fg="#ffffff")
    widget_text.pack(side="left", fill='both', expand=True)

    scrollbar = tk.Scrollbar(console)
    scrollbar.pack(side='right', fill='y')

    widget_text['yscrollcommand'] = scrollbar.set
    scrollbar['command'] = widget_text.yview
   
    old_stdout = sys.stdout
    sys.stdout = Redirect(widget_text)
    # sys.stdout = old_stdout #uncomment if you want normal terminal output
    return console


#Example code
def test():
    print("Hello Gui!")
    enable_disable_console()
    window.after(100, test2)

def test2():
    print("Hello Console!")
    enable_disable_console()
    window.after(100, test)

if __name__ == '__main__':
    window = tk.Tk()
    window.geometry("1920x1080")
    Terminal = console_init(window)
    Terminal.place(relx=1, rely=0.80, relheight=0.8, relwidth=0.40, anchor='se')
    autoscroll_button = tk.Button(window, text="Toggle console autoscroll", height=5, width=20, command=toggle_autoscroll)
    autoscroll_button.place(relx=0.8, rely=1, relheight=0.1, relwidth=0.1, anchor='se')                              
    window.after(1000, test)
    window.mainloop()
