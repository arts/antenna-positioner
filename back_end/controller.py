from copy import deepcopy
from threading import Thread, Event

from back_end.model.config.settings import Settings
from back_end.model.config import config
from back_end.model.pdf import pdf_module
from front_end import antenna_positioner_gui
from back_end.model import measurement
import os

global settings, pdf_settings, running


def init():
    """Entry point of the application, inits the GUI, config and some variables needed for the program to function"""
    global settings, pdf_settings, running
    running = Event()
    settings = Settings()
    pdf_settings = deepcopy(settings)
    config.init(settings)
    antenna_positioner_gui.main(settings, start_measurement, stop_measurement, generate_pdf)


def start_measurement(demo_mode=False, path=""):
    """Starts a measurement and disables the GUI buttons, takes demo_mode toggle boolean and export path"""
    global settings, running, pdf_settings
    if demo_mode:
        set_demo_mode()
        print(
            "Note Demo mode uses predefined data.\nThus any azimuth or elevation outside of this index will result in "
            "an index out of range error\nSee file: back-end/model/plot/hoofdpijn_dictionary.py for indexes.")
    antenna_positioner_gui.start_measurement(settings)
    running = Event()
    running.set()
    thread = Thread(target=measurement.start_measurement_backend,
                    args=(
                        running, settings, antenna_positioner_gui.enable_buttons, measurement_stopped, demo_mode, path))
    thread.start()


def stop_measurement():
    """Stops the measurement thread"""
    global running
    running.clear()
    measurement_stopped()


def measurement_stopped():
    """To be called when a measurement comes to an end, either by user intervention or any other cause.
    Enables GUI buttons and creates copy of used settings for the pdf generator"""
    global settings, pdf_settings
    antenna_positioner_gui.enable_buttons()
    pdf_settings = deepcopy(settings)


def generate_pdf(path):
    """Starts a new thread which generates a pdf of the measurement to the given path"""
    global settings, pdf_settings
    thread = Thread(target=generate_surface_plot_png, args=(path,))
    thread.start()


def set_demo_mode():
    """Function used to switch the current preset to a demo preset when using demo mode"""
    if 'demo' not in config.get_presets():
        config.add_preset('demo',
                          power_level_dBm=5,
                          bandwidth_kHz=0.1,
                          start_frequency=1000,
                          stop_frequency=2000,
                          steps_in_frequency_test_band=21,
                          start_el=-90.0,
                          stop_el=90.0,
                          el_steps=37,
                          start_az=-100.0,
                          stop_az=100.0,
                          az_steps=41)
        config.update_config_file()
    config.switch_current_preset('demo')


def generate_surface_plot_png(path="exports"):
    """Generates all the pngs of the surface plots and puts them in a pdf file"""
    global settings, pdf_settings
    dir = 'exports\surfaceplot_buffer'
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))
    antenna_positioner_gui.generate_png(r'exports\surfaceplot_buffer')
    # for n in object_list:
    #    Plot.draw_png(n, filepath=r'exports\surfaceplot_buffer')
    print(pdf_settings)
    pdf_module.generate_pdf(antenna_positioner_gui,
                            'Positioner_Report_' + pdf_settings.user_parameters.project_name + '_',
                            pdf_settings.user_parameters.company_name,
                            pdf_settings.user_parameters.operator,
                            pdf_settings.user_parameters.antenna_name,
                            pdf_settings.user_parameters.description,
                            pdf_settings.current_preset.start_frequency,
                            pdf_settings.current_preset.stop_frequency,
                            'MHz',
                            pdf_settings.current_preset.steps_in_frequency_test_band,
                            (
                                    pdf_settings.current_preset.stop_frequency - pdf_settings.current_preset.start_frequency) / (
                                    pdf_settings.current_preset.steps_in_frequency_test_band - 1),
                            'MHz',
                            pdf_settings.current_preset.bandwidth_kHz,
                            pdf_settings.current_preset.power_level_dBm,
                            pdf_settings.current_preset.start_az,
                            pdf_settings.current_preset.stop_az,
                            pdf_settings.current_preset.az_steps,
                            pdf_settings.current_preset.start_el,
                            pdf_settings.current_preset.stop_el,
                            pdf_settings.current_preset.el_steps,
                            path)
