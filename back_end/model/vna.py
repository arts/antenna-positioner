"""
VNA
"""

import time

from RsInstrument import ResourceError
from RsInstrument.RsInstrument import RsInstrument
import back_end.model.utils as utils


class VNA:

    def __init__(self, vna_settings, current_preset, demo = False):
        print("VNA[", vna_settings.name, "] init")
        RsInstrument.assert_minimum_version('1.10.0')

        self.instr = None

        self.name = vna_settings.name
        self.ip_address = 'TCPIP::' + vna_settings.ip_address + '::INSTR'

        self.settings = current_preset
        self.channels = vna_settings.ports
        self.ground_port = vna_settings.tx_port
        if demo:
            return
        try:
            # -----------------------------------------------------------
            # Initialization:
            # -----------------------------------------------------------
            # Adjust the VISA Resource string to fit your instrument
            self.instr = RsInstrument(self.ip_address, True, False)
            print('Reset the instrument, clear the Error queue, clear screen')
            self.instr.write_str("*RST")  # Reset the instrument, clear the Error queue
            self.instr.write_str("*CLS")  # Clear
            self.instr.write_str("SYSTEM:DISPLAY:UPDATE ON")

            idn = self.instr.query_str('*IDN?')
            print(f"\nVNA is a '{idn}'")

            self.instr.visa_timeout = 3000  # Timeout for VISA Read Operations
            self.instr.instrument_status_checking = True  # Error check after each command

        except ResourceError as ex:
            print('Error initializing the instrument session:\n' + ex.args[0])
            #exit() #Fuck You!

    def command(self, cmd):
        self.instr.write_str(cmd)

    def set_settings(self):
        """Settings"""
        try:
            print('VNA[', self.name, '] Settings')

            self.instr.write_str("SWEep:TYPE LINear")
            step = (self.settings.stop_frequency - self.settings.start_frequency) / (self.settings.steps_in_frequency_test_band - 1)
            self.instr.write_str("SWEep:STEP " + str(step) + "MHz")

            self.instr.write_str("BANDwidth " + str(self.settings.bandwidth_kHz) + "kHz")

            self.instr.write_str("SOURce:POWer:LEVel " + str(self.settings.power_level_dBm) + "dBm")

            for channel in self.channels:
                self.instr.write_str('SENSe' + channel + ':FREQuency:START ' +
                                     str(self.settings.start_frequency) + "MHz")
                self.instr.write_str('SENSe' + channel + ':FREQuency:STOP ' +
                                     str(self.settings.stop_frequency) + "MHz")
                self.instr.write_str('SENSe' + channel + ':SWEep:POINts ' +
                                     str(self.settings.steps_in_frequency_test_band))

        except Exception as ex:
            print('Error setting:\n' + ex.args[0])

    def implement_instrumentstate_file(self, filename):
        cmd = "SYSTem:PRESet:USER:NAME '" + filename + "'"
        print('VNA[', self.name, '] implement_instrumentstate_file cmd=', cmd)
        self.instr.write_str(cmd)
        self.instr.write_str("SYSTem:PRESet:USER:STATe ON")

    def set_screen(self, channel):
        """Set Screen"""
        try:
            print('VNA[', self.name, '] Set screen for S' + str(channel) + '1')
            print("CALCulate1:PARameter:SDEFine 'TRC2', 'S21'")
            print("CALCulate1:PARameter:SDEFine 'TRC" + str(channel) + "', 'S" + str(channel) + "1'")
            self.instr.write_str("CALCulate1:PARameter:SDEFine 'TRC" + str(channel) + "', 'S" + str(channel) + "1'")
            self.instr.write_str("CALCulate1:FORMat MLOG")
            self.instr.write_str("DISPlay:WINDow" + str(channel) + ":STATe ON")
            self.instr.write_str("DISPlay:WINDow" + str(channel) + ":TRACe:FEED 'TRC" + str(channel) + "'")
        except Exception as ex:
            print('Error setting:\n' + ex.args[0])

    def init_sweep(self):
        """Sweep"""
        try:
            print('VNA[', self.name, '] Init Sweep')
            self.instr.write_str("INIT:CONT OFF")  # Switch OFF the continuous sweep
            self.instr.write_str("INIT")
            time.sleep(0.1)
            retries = 0
            while True:
                opc = int(self.instr.query_str('*OPC?'))
                # print(f"\nVNA opc: '{opc}'")
                if opc == 1:
                    break

                retries = retries + 1
                if retries > 10:
                    print('VNA[', self.name, '] Start ERROR')
                    break
        except Exception as ex:
            print('Error setting:\n' + ex.args[0])

    def sweep(self):
        """Start"""
        try:
            print('VNA[', self.name, '] Sweep')
            opc = int(self.instr.query_str("INITiate1:IMMEdiate;*OPC?"))
            retries = 0
            # print(f"\nVNA opc: '{opc}'")
            while opc != 0:
                opc = int(self.instr.query_str('*OPC?'))
                # print(f"\nVNA opc: '{opc}'")
                if opc == 1:
                    break

                retries = retries + 1
                if retries > 10:
                    print('VNA[', self.name, '] Start ERROR')
                    break

        except Exception as ex:
            print('Error setting:\n' + ex.args[0])

    def get_status(self):
        """Status"""
        try:
            stat = self.instr.query_str('SYSTEM:ERROR?')
            print(f"\nVNA[{self.name}]  status: '{stat}'")
        except Exception as ex:
            print('Error setting:\n' + ex.args[0])

    def read_channel(self, channel):
        """read_channel"""
        try:
            self.instr.write_str("CALC1:PAR:SEL 'TRC" + str(channel) + "'")
            r = self.instr.query_str('CALC1:DATA? SDAT')
            return utils.ascii_to_floats(r)
        except Exception as ex:
            print('Error setting:\n' + ex.args[0])
