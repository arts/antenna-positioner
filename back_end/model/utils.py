import numpy as np


def complex_to_magn_phase(c):
    # print(c)
    magn = 20.0 * np.log10(np.sqrt((c[0] ** 2) + (c[1] ** 2)))
    cc = c[0] + 1j * c[1]
    phase = np.angle(cc, deg=True)
    return magn, phase


def array_complex_to_magn_phase(ca):
    arr = []
    # print("in:",ca)
    for idx in range(0, len(ca), 2):
        mp = complex_to_magn_phase((ca[idx], ca[idx + 1]))
        arr.append(mp[0])
        arr.append(mp[1])
    # print("out:",arr)
    return arr


def ascii_to_floats(r):
    rs = r.split(",")
    result = []
    for rsr in rs:
        result.append(float(rsr))
    return result


def select_points(points, selection):
    sels = []
    si = 0
    for s in selection:
        if s:
            sels.append(points[si])
        si = si + 1
    return sels


def az_to_raw(ph, az):
    return round(ph * (360.0 + az))


def el_to_raw(pv, el):
    return round(pv * (360.0 + el))


def raw_to_az(a):
    return (a[1] * 100 + a[2] * 10 + a[3] + a[4] * 0.1) - 360.0


def raw_to_el(a):
    return (a[6] * 100 + a[7] * 10 + a[8] + a[9] * 0.1) - 360.0
