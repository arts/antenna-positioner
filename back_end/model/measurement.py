import time
from copy import deepcopy

import numpy as np
import csv

from back_end.model.config.settings import Settings
from back_end.model.config import config
from back_end.model.vna import VNA
from back_end.model.pos import Pos
from back_end.model.plot import plot
from back_end.model import utils

global pos, running


def start_measurement_backend(running, settings, enable_buttons, measurement_stopped, demo=False, path="exports"):
    global pos
    scp = settings.current_preset
    filename = path + "\GRATE_POS_VNA_" + time.strftime("%Y%m%d-%H%M") + ".txt"
    filenamecsv = path + "\GRATE_POS_VNA_" + time.strftime("%Y%m%d-%H%M") + ".csv"
    f = open(filename, "a")
    csv_file = open(filenamecsv, 'w', newline='')
    gratecsv = csv.writer(csv_file, dialect='excel', delimiter=';')
    gratecsv.writerow(['VNA'] + ['Port'] + ['Az'] + ['El'] + ['Magnituder/Phase'] + [str(n) + "MHz" for n in range(scp.start_frequency, int(scp.stop_frequency + (scp.stop_frequency-scp.start_frequency)/(scp.steps_in_frequency_test_band-1)), int((scp.stop_frequency-scp.start_frequency)/(scp.steps_in_frequency_test_band-1)))])
    print("# GRATE Positioner-VNA measurement starting. Output file=", filename, file=f)
    print("# mp = magnitude (index 0,2,4,...) , phase (index 1,3,5,...)", file=f)
    output_string = "# az, el"
    if not demo:
        try:
            pos = Pos(settings.positioner)
            print(scp)
            vnas = [VNA(settings.vnas[vna_settings], scp) for vna_settings in settings.vnas]
            for vna in vnas:
                for channel in vna.channels:
                    vna.set_screen(channel)
                vna.set_settings()
                vna.init_sweep()
                vna.get_status()
                print("Doing stuff with VNA")
            print("VNA's are READY to go")
            time.sleep(1)
            for vna in vnas:
                for channel in vna.channels:
                    if channel == vna.ground_port:
                        continue
                    output_string = output_string + ", [output_mp[" + vna.name + "][" + channel + "]]"
            print(output_string, file=f)

            print("Move to center:")
            (ret, az, el) = pos.write_position(0, 0, running)
            actual_az = az
            actual_el = el

        except Exception as Ex:
            print("Something went wrong during Init of VNA/POS Objects:")
            print(Ex)
            return

            # uncomment the following line when only moving to 0,0:
            # exit()
    else:
        from back_end.model.plot.hoofdpijn_dictionary import test_dictionary as TD
        vnas = [VNA(settings.vnas[vna_settings], scp, demo=True) for vna_settings in settings.vnas]
    dir = 1

    # User settings:
    start_el = scp.start_el
    stop_el = scp.stop_el
    step_el = scp.el_steps

    start_az = scp.start_az
    stop_az = scp.stop_az
    step_az = scp.az_steps

    vna_repeats = 1

    # about 1 second per degree
    if start_el > stop_el:
        difference = abs(start_el - stop_el)
    else:
        difference = abs(stop_el - start_el)
    time_estimate = difference * step_az + (step_az * step_el * 0.6)
    print("Estimated total time for measurement: ", str(time_estimate / 60), " minutes")
    # WHY?

    # generate list for finding data position
    list_az = np.arange(scp.start_az,
                        scp.stop_az + (
                                    scp.stop_az - scp.start_az) / (scp.az_steps-1),
                        (
                                    scp.stop_az - scp.start_az) / (scp.az_steps-1)).tolist()
    list_el = np.arange(scp.start_el,
                        scp.stop_el + (
                                    scp.stop_el - scp.start_el) / (scp.el_steps-1),
                        (
                                    scp.stop_el - scp.start_el) / (scp.el_steps-1)).tolist()
    print(list_az)
    print(list_el)
    for el in list_el:
        if not running.is_set():
            break
        for az in list_az:
            if not running.is_set():
                break

            az_dir = az * dir

            # az_dir_step = az_dir + dir
            # POS_Device.write_position(az_dir_step, el)

            print("dir: " + str(az_dir))
            print("el/az: " + str(el) + " " + str(az))

            if not demo:
                (ret, act_az, act_el) = pos.write_position(az_dir, el, running)
                actual_az = act_az
                actual_el = act_el
            else:
                actual_az = az
                actual_el = el
            timeout = 30
            print("actual el/az: " + str(actual_el) + " " + str(actual_az))
            output = {}
            output_mp = {}
            for vna in vnas:
                output[vna.name] = {}
                output_mp[vna.name] = {}
                for channel in vna.channels:
                    output[vna.name][channel] = []
                    output_mp[vna.name][channel] = []
            for rep in range(vna_repeats):
                test = 0
                for vna in vnas:
                    print("VNA[", vna.name, "] measure NOW!")
                    for channel in vna.channels:
                        if channel == vna.ground_port:
                            continue
                        if not demo:
                            vna.sweep()
                            output[vna.name][channel] = vna.read_channel(channel)
                            output_mp[vna.name][channel] = utils.array_complex_to_magn_phase(output[vna.name][channel])
                            # plot.addpoint(i, 0, actual_az, actual_el, r21mp[i][8])
                        else:
                            try:
                                # print(vna.name, channel, az, el, test)
                                # print(TD[str(az), str(el)][test])
                                if str(az_dir) == '-0.0':
                                    output_mp[vna.name][channel] = TD['0.0', str(el)][test]
                                else:
                                    output_mp[vna.name][channel] = TD[str(az_dir), str(el)][test]
                                test = test + 1
                            except (KeyError, IndexError) as error:
                                if "index" in str(error):
                                    print("IndexError", error)
                                else:
                                    print("KeyError key:", error, "was not in test_dictionary.")
                                print("Please see file: back-end/model/plot/hoofdpijn_dictionary.py for indexes.")
                                f.close()
                                csv_file.close()
                                enable_buttons()
                                measurement_stopped()
                                return

            output_string = f"{actual_az}, {actual_el}"
            port = 0
            for vna in vnas:
                for channel in vna.channels:
                    if channel == vna.ground_port:
                        continue
                    output_string = output_string + f", {output_mp[vna.name][channel]}"

                    gratecsv.writerow([vna.name] + [channel] + [actual_az] + [actual_el] + ['Magnitude'] + [str(output_mp[vna.name][channel][freq]).replace('.',',') for freq in range(0, 2 * scp.steps_in_frequency_test_band, 2)])
                    gratecsv.writerow([vna.name] + [channel] + [actual_az] + [actual_el] + ['Phase'] + [str(output_mp[vna.name][channel][phase]).replace('.',',') for phase in range(1, 2 * scp.steps_in_frequency_test_band + 1, 2)])
                    for freq in range(0, 2 * scp.steps_in_frequency_test_band, 2):
                        # print("Adding Data to PLOT object:", int(freq / 2 + port * scp.steps_in_frequency_test_band))
                        plot.Plot.add_measurement(plot.object_list[
                                                      int(freq / 2 + port * scp.steps_in_frequency_test_band)],
                                                  list_az.index(az_dir),
                                                  list_el.index(el),
                                                  output_mp[vna.name][channel][freq])
                    port = port + 1

                # print("write to file:")
                # print(output_string)
                print(output_string, file=f)
                # if gui:
                # gui.surface_plot_gui()
        time.sleep(0.1)
        dir = -dir

    # pdf_data = deepcopy(output_mp)
    if not demo and running.is_set():
        print("Move back to center:")
        pos.write_position(0, 0, running)
    enable_buttons()
    measurement_stopped()
    print("Finished")
    f.close()
    csv_file.close()
