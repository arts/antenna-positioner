import configparser
import os
import re

from back_end.model.config.settings import Vna, Preset, CurrentPreset, Settings

global settings, file_name, cp


def init(glob_settings, glob_file_name='config.ini'):
    """Inits the variables used by the config.py file.
    Takes a settings object to fill and a path to the .ini file to read."""
    global file_name, cp, settings
    settings = glob_settings
    file_name = glob_file_name
    cp = configparser.ConfigParser()
    init_settings()


def init_settings():
    """Fills the settings object with data from the .ini file and creates one if it doesn't exist yet"""
    global cp
    if not os.path.exists(file_name):
        #  creates default config as defined by settings dataclass, fills in all but vna.py and preset data
        update_config_file()
    read_settings()


def read_settings(preset='default'):
    """Reads all the settings from the .ini file, takes a preset to set as current_preset"""
    global settings
    cp.read(file_name)
    # read positioner settings
    settings.positioner.ip = cp['positioner']['ip_address']
    settings.positioner.port = cp['positioner']['tcp_port']
    settings.positioner.ph = cp['positioner']['ph']
    settings.positioner.pv = cp['positioner']['pv']
    settings.positioner.offset_el = cp['positioner']['offset_el']
    settings.positioner.offset_az = cp['positioner']['offset_az']

    # read user parameters
    settings.user_parameters.project_name = cp['user_parameters']['project_name']
    settings.user_parameters.antenna_name = cp['user_parameters']['antenna_name']
    settings.user_parameters.operator = cp['user_parameters']['operator']
    settings.user_parameters.company_name = cp['user_parameters']['company']
    settings.user_parameters.description = cp['user_parameters']['description']

    for section in cp.sections():
        if 'vna_' in section:  # read vna.py settings
            settings.vnas[section.replace('vna_', '')] = Vna(
                name=cp[section]['name'],
                ip_address=cp[section]['ip_address'],
                tx_port=cp[section]['tx_port'],
                ports=list(cp[section]['ports'].split(", ")))

        elif 'test_parameters_' in section:  # read test parameters settings
            settings.presets[section.replace('test_parameters_', '')] = Preset(
                power_level_dBm=int(cp[section]['power_level_dBm']),
                bandwidth_kHz=float(cp[section]['bandwidth_kHz']),
                start_frequency=int(cp[section]['start_frequency']),
                stop_frequency=int(cp[section]['stop_frequency']),
                steps_in_frequency_test_band=int(cp[section]['steps_in_frequency_test_band']),
                start_el=float(cp[section]['start_el']),
                stop_el=float(cp[section]['stop_el']),
                el_steps=int(cp[section]['el_steps']),
                start_az=float(cp[section]['start_az']),
                stop_az=float(cp[section]['stop_az']),
                az_steps=int(cp[section]['az_steps']))

    missing = False
    if not settings.vnas:  # adds default vna.py data if missing from config
        settings.vnas['1'] = Vna()
        missing = True

    if 'default' not in settings.presets.keys():  # adds default preset data if missing from config
        settings.presets['default'] = Preset()
        missing = True

    if missing:
        update_config_file()

    switch_current_preset(preset, init=True)


def update_config_file():
    """Updates the config file with the values in the settings object"""
    global settings
    cp = configparser.ConfigParser()

    # update positioner parameters
    cp['positioner'] = {'ip_address': settings.positioner.ip_address,
                        'tcp_port': settings.positioner.tcp_port,
                        'ph': settings.positioner.ph,
                        'pv': settings.positioner.pv,
                        'offset_el': settings.positioner.offset_el,
                        'offset_az': settings.positioner.offset_az}

    # update vnas
    for k, v in settings.vnas.items():
        ports_string = re.sub("[\\[\\]']", '', str(v.ports))
        cp['vna_' + k] = {'name': v.name,
                          'ip_address': v.ip_address,
                          'tx_port': v.tx_port,
                          'ports': ports_string}

    # update user parameters
    cp['user_parameters'] = {'project_name': settings.user_parameters.project_name,
                             'antenna_name': settings.user_parameters.antenna_name,
                             'operator': settings.user_parameters.operator,
                             'company': settings.user_parameters.company_name,
                             'description': settings.user_parameters.description}

    # update presets
    for k, v in settings.presets.items():
        cp['test_parameters_' + k] = {'power_level_dBm': v.power_level_dBm,
                                      'bandwidth_kHz': v.bandwidth_kHz,
                                      'start_frequency': v.start_frequency,
                                      'stop_frequency': v.stop_frequency,
                                      'steps_in_frequency_test_band': v.steps_in_frequency_test_band,
                                      'start_el': v.start_el,
                                      'stop_el': v.stop_el,
                                      'el_steps': v.el_steps,
                                      'start_az': v.start_az,
                                      'stop_az': v.stop_az,
                                      'az_steps': v.az_steps
                                      }

    with open(file_name, 'w') as file:
        cp.write(file)


def switch_current_preset(preset, init=False):
    """Switches the current preset to the given preset if it exists in the preset list in the settings object.
    Updates the values of the previous preset if init equals False."""
    global settings
    if preset not in settings.presets.keys():
        raise ValueError("Preset doesn't exist")
    if not init:
        update_current_preset()
    settings.current_preset = CurrentPreset(parent=settings.presets[preset], preset_name=preset)


def update_current_preset():
    """Updates the preset currently used as current_preset in the preset list in the settings object"""
    settings.presets[settings.current_preset.preset_name] = Preset(

        power_level_dBm=settings.current_preset.power_level_dBm,
        bandwidth_kHz=settings.current_preset.bandwidth_kHz,
        start_frequency=settings.current_preset.start_frequency,
        stop_frequency=settings.current_preset.stop_frequency,
        steps_in_frequency_test_band=settings.current_preset.steps_in_frequency_test_band,
        start_el=settings.current_preset.start_el,
        stop_el=settings.current_preset.stop_el,
        el_steps=settings.current_preset.el_steps,
        start_az=settings.current_preset.start_az,
        stop_az=settings.current_preset.stop_az,
        az_steps=settings.current_preset.az_steps)


def add_preset(preset, power_level_dBm, bandwidth_kHz, start_frequency, stop_frequency,
               steps_in_frequency_test_band, start_el, stop_el, el_steps, start_az, stop_az,
               az_steps):
    """Adds a preset to the preset list in the settings object if it doesn't exist yet"""
    if preset in settings.presets.keys():
        raise ValueError("Preset already exist")
    settings.presets[preset] = Preset(
        power_level_dBm=power_level_dBm,
        bandwidth_kHz=bandwidth_kHz,
        start_frequency=start_frequency,
        stop_frequency=stop_frequency,
        steps_in_frequency_test_band=steps_in_frequency_test_band,
        start_el=start_el,
        stop_el=stop_el,
        el_steps=el_steps,
        start_az=start_az,
        stop_az=stop_az,
        az_steps=az_steps)


def remove_preset(preset):
    """Removes a preset from the preset list if it exists"""
    global settings
    cp.read(file_name)
    if preset == 'default':
        raise ValueError("Default preset cannot be removed")
    if preset not in settings.presets.keys():
        raise ValueError("Preset doesn't exist")
    del settings.presets[preset]


def get_presets():
    """Returns the available presets"""
    global settings
    return settings.presets.keys()


def add_vna(vna, name, ip_address, tx_port, ports):
    """Adds a new vna to the vna list in the settings object"""
    global settings
    if vna in settings.vnas.keys():
        raise ValueError("Vna already exists")
    settings.vnas[vna] = Vna(name=name, ip_address=ip_address, tx_port=tx_port, ports=ports)


def remove_vna(vna):
    """Removes the given vna from the vna list in the settings object if it exists"""
    global settings
    if vna not in settings.vnas.keys():
        raise ValueError("Vna doesn't exist")
    del settings.vnas[vna]


def get_vnas():
    """Returns the available vnas"""
    global settings
    return settings.vnas.keys()


def get_vna_port_names(vna):
    """Returns a list of the vna name + the ports for every port available except the tx_port"""
    return [vna.name + "_P" + str(port) for port in vna.ports if port is not vna.tx_port]


def get_all_vna_port_names():
    """Returns a list of all the vna names + their ports except the tx ports"""
    port_list = []
    for vna in get_vnas():
        port_list.extend(get_vna_port_names(settings.vnas[vna]))
    return port_list


if __name__ == "__main__":  # example usage
    settings = Settings()  # settings object should be shared across entire application
    init(settings)  # load settings object with values from config, creates config if it doesn't yet exist
    try:
        remove_preset('default')  # default preset cannot be removed
    except ValueError as error:
        print(error)

    try:
        add_preset('test_preset', 0.1, 6, 2, 3, 31, -90, 90, 10, -100, 100, 10)  # add preset
        add_preset('another_test_preset', 0.2, 2, 2, 3, 31, -90, 90, 10, -100, 100, 10)  # add preset
        print('Current presets' + str(get_presets()))
        add_vna('test_vna', 'some_name', '1.2.3.4', 1, [2, 4])  # add vna
        remove_preset('test_preset')  # removing preset
        print('Current presets: ' + str(get_presets()))
        remove_vna('1')  # removing vna
    except ValueError as error:
        print(error)

    switch_current_preset('another_test_preset')  # set current preset
    print('Current preset: ' + str(settings.current_preset))
    settings.user_parameters.description = 'een project ofzo'  # modifying regular params
    settings.vnas['test_vna'].name = 'modified_some_name'  # modifying vna params
    settings.current_preset.stop_frequency = 6  # modifying preset params
    update_current_preset()  # should be called after modifying settings.current_preset
    update_config_file()  # config.ini file won't be updated with new modifications unless this function is called
