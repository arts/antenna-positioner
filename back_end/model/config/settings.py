from dataclasses import dataclass, field

from typing import Dict, List


@dataclass
class Positioner:
    ip_address: str = '10.77.0.23'
    tcp_port: int = 23
    ph: int = 10
    pv: int = 10
    offset_el: int = 90
    offset_az: int = 180


@dataclass
class Vna:
    name: str = 'ZWO 2000'
    ip_address: str = '10.77.0.21'
    tx_port: int = 1
    ports: List[int] = field(default_factory=lambda: [1, 2, 3, 4])


@dataclass
class UserParameters:
    project_name: str = 'Project Name'
    antenna_name: str = "Antenna name"
    operator: str = 'Operator'
    company_name: str = 'ASTRON'
    description: str = ''


@dataclass
class Preset:
    power_level_dBm: int = 5
    bandwidth_kHz: float = 0.1
    start_frequency: int = 1000
    stop_frequency: int = 2000
    steps_in_frequency_test_band: int = 21
    start_el: float = -90
    stop_el: float = 90
    el_steps: int = 5
    start_az: float = -100
    stop_az: float = 100
    az_steps: int = 5


@dataclass(init=False)
class CurrentPreset(Preset):  # extends Preset
    preset_name: str = 'default'

    def __init__(self, parent=Preset(), preset_name='default'):
        self.preset_name = preset_name
        self.start_frequency = parent.start_frequency
        self.stop_frequency = parent.stop_frequency
        self.steps_in_frequency_test_band = parent.steps_in_frequency_test_band
        self.start_el = parent.start_el
        self.stop_el = parent.stop_el
        self.el_steps = parent.el_steps
        self.start_az = parent.start_az
        self.stop_az = parent.stop_az
        self.az_steps = parent.az_steps


@dataclass
class Settings:
    positioner: Positioner = Positioner()
    vnas: Dict[str, Vna] = field(default_factory=dict)  # dict of Vna dataclass objects
    user_parameters: UserParameters = UserParameters()
    presets: Dict[str, Preset] = field(default_factory=dict)  # dict of Preset dataclass objects
    current_preset = CurrentPreset()
