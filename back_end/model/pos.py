"""
pos Class for Rot2Prog

See documentation: http://ryeng.name/blog/3
"""

import socket
import time

import back_end.model.utils as utils


# TODO define max rotation
class Pos:
    def __init__(self, positioner_settings):
        self.ip_address = positioner_settings.ip_address
        self.tcp_port = positioner_settings.tcp_port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.ip_address, self.tcp_port))
        self.connected = True

        self.PH = positioner_settings.ph
        self.PV = positioner_settings.pv

        self.offset_az = positioner_settings.offset_az
        self.offset_el = positioner_settings.offset_el
        print("Offset AZ " + str(positioner_settings.offset_az))
        print("Offset EL " + str(positioner_settings.offset_el))


    def reconnect(self):
        self.sock.close()
        self.connected = False
        print("connection lost...")
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        while not self.connected:
            try:
                print("reconnecting")
                self.sock.connect((self.ip_address, self.tcp_port))
                self.connected = True
                print("reconnect successful")
            except socket.error:
                time.sleep(2)

    def read_bytes(self, bytes_to_read):
        try:
            r = self.sock.recv(bytes_to_read)
            # print("read_bytes:",len(r))
            return True, r
        except socket.error:
            self.reconnect()
            return False, bytes([])

    def write_bytes(self, data):
        try:
            self.sock.sendall(data)
            time.sleep(0.25)
            return True
        except socket.error:
            self.reconnect()
            return False

    def command_status(self):
        return bytes([0x57, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x20])

    def command_stop(self):
        return bytes([0x57, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x20])

    def print_response(self, r):
        print("response raw=", list(r))
        az = utils.raw_to_az(r) - self.offset_az
        el = utils.raw_to_el(r) - self.offset_el
        print("read_position az=", str(az), " el=", str(el))

    def read_position(self):
        old_r = []
        retries = 0

        while True:
            if not self.write_bytes(self.command_status()):
                return False, 0, 0

            (ret, r) = self.read_bytes(1200)
            if not ret:
                return False, 0, 0

            a = utils.raw_to_az(r) - self.offset_az
            e = utils.raw_to_el(r) - self.offset_el

            # print("MOVING to ", az, ",", el, " NOW=", a, ",", e, " (raw=", list(r), "raw_to_az/el=",
            # self.raw_to_az(r), ",", self.raw_to_el(r), " time: ", time.time() - tic)

            if r == old_r:
                retries = retries + 1
                if retries > 100:
                    print("Unchanged position!")
                    return False, a, e
                time.sleep(0.25)
                continue
            old_r = r
            time.sleep(0.25)
            return True, a, e

    def wait_for_position(self, az, el, stop_event, timeout):
        tic = time.time()
        a = az
        e = el
        while True:
            (ret, a, e) = self.read_position()
            if not stop_event.is_set():
                self.write_bytes(self.command_stop())
                return False, a, e
            if not ret:
                return False, a, e

            print(
                "POSITION=(%.02f, %.02f)   MOVE to (%.02f, %.02f)  time=%.02f sec" % (a, e, az, el, time.time() - tic))

            if (int(a) == int(az)) and (int(e) == int(el)):
                break
            toc = time.time()
            if toc > (tic + timeout):
                print("Timeout...")
                return False, a, e

            time.sleep(0.5)
        toc = time.time()
        print("Moving took: %f sec" % (toc - tic))
        return True, a, e

    def write_position(self, az, el, stop_event, timeout=120):
        if az > 100:
            az = 100
        if az < -100:
            az = -100

        if el > 90:
            el = 90
        if el < -89:  # -90
            el = -89  # -90

        a = az
        e = el

        az_offs = az + self.offset_az
        el_offs = el + self.offset_el
        print("MOVE to az,el=", str(az), ",", str(el), "(+offs=", str(az_offs), ",", str(el_offs), ")")
        cmd = [0x57, 0x00, 0x00, 0x00, 0x00, self.PH, 0x00, 0x00, 0x00, 0x00, self.PV, 0x2F, 0x20]
        az_raw = utils.az_to_raw(self.PH, az_offs)
        el_raw = utils.el_to_raw(self.PV, el_offs)
        # print("(az_raw=", az_raw, " el_raw=", el_raw,")")
        cmd[1] = int((az_raw % 10000) / 1000) | 0x30
        cmd[2] = int((az_raw % 1000) / 100) | 0x30
        cmd[3] = int((az_raw % 100) / 10) | 0x30
        cmd[4] = int(az_raw % 10) | 0x30

        cmd[6] = int((el_raw % 10000) / 1000) | 0x30
        cmd[7] = int((el_raw % 1000) / 100) | 0x30
        cmd[8] = int((el_raw % 100) / 10) | 0x30
        cmd[9] = int(el_raw % 10) | 0x30

        for retry in range(100):
            if not stop_event.is_set():
                break
            if not self.write_bytes(bytes(cmd)):
                time.sleep(0.25)
                continue

            (ret, r) = self.read_bytes(1200)
            if not ret:
                time.sleep(0.25)
                continue

            (ret, a, e) = self.wait_for_position(az, el, stop_event, timeout)
            if ret:
                return self.read_position()  # send final position
            print("Retrying! Force stop first")
            if not self.write_bytes(self.command_stop()):
                time.sleep(0.25)
                continue

            (ret, r) = self.read_bytes(1200)
            if not ret:
                time.sleep(0.25)
                continue

            self.print_response(r)

        print("write_position FAILED !!!!")
        return False, a, e
