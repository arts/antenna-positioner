Company:	{}
Operator:	{}
DateTime:	{}

Type: {}
Description:
{}

Frequency range:    {:.2f}/{:.2f}{}
Frequency steps:	{}Int|{:.2f}{}
Bandwidth:          {}KHz
Power level:        {}DBm

Azimuth range:		{:.2f}/{:.2f}Degrees
Azimuth steps:		{}Int

Elevation range:	{:.2f}/{:.2f}Degrees
Elevation steps:	{}Int