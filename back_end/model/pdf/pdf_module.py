from fpdf import FPDF
import os
import time

pdf_title = ''


class Pdf(FPDF):
    def header(self):
        # Logo
        self.image('media/Astron_Logo_m1000.jpg', 10, 8, 100)
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        # Move to the right
        self.cell(80)
        # title
        self.cell(30, 10, pdf_title, 0, 0, 'C')
        # Line break
        self.ln(20)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

    def chapter_title(self, num, label):
        # Arial 12
        self.set_font('Arial', '', 12)
        # Background color
        self.set_fill_color(200, 220, 255)
        # title
        self.cell(0, 6, 'Chapter %d : %s' % (num, label), 0, 1, 'L', True)
        # Line break
        self.ln(4)

    def chapter_body(self,
                     name,
                     local_company,
                     local_author,
                     local_antenna_type,
                     local_description,
                     local_frequency_start,
                     local_frequency_stop,
                     local_frequency_start_stop_unit,
                     local_frequency_step_int,
                     local_frequency_step_hz,
                     local_frequency_step_unit,
                     bandwidth,
                     power_level,
                     local_azimuth_start,
                     local_azimuth_stop,
                     local_azimuth_step,
                     local_elevation_start,
                     local_elevation_stop,
                     local_elevation_step):
        # Read text file
        with open(name, 'rb') as fh:
            # add test data to string
            txt = fh.read().decode('latin-1').format(local_company,
                                                     local_author,
                                                     time.strftime("%d-%m-%Y %H:%M"),
                                                     local_antenna_type,
                                                     local_description,
                                                     local_frequency_start,
                                                     local_frequency_stop,
                                                     local_frequency_start_stop_unit,
                                                     local_frequency_step_int,
                                                     local_frequency_step_hz,
                                                     local_frequency_step_unit,
                                                     bandwidth,
                                                     power_level,
                                                     local_azimuth_start,
                                                     local_azimuth_stop,
                                                     local_azimuth_step,
                                                     local_elevation_start,
                                                     local_elevation_stop,
                                                     local_elevation_step)
        # Times 12
        self.set_font('Arial', '', 12)
        # Output justified text
        self.multi_cell(0, 5, txt)
        # Line break
        self.ln()
        # Mention in italics
        self.set_font('', 'I')
        self.cell(0, 5, '')

    # generates first chapter
    def print_chapter(self,
                      num,
                      title,
                      name,
                      local_company,
                      local_author,
                      local_antenna_type,
                      local_description,
                      local_frequency_start,
                      local_frequency_stop,
                      local_frequency_start_stop_unit,
                      local_frequency_step_int,
                      local_frequency_step_hz,
                      local_frequency_step_unit,
                      bandwidth,
                      power_level,
                      local_azimuth_start,
                      local_azimuth_stop,
                      local_azimuth_step,
                      local_elevation_start,
                      local_elevation_stop,
                      local_elevation_step):
        self.add_page()
        self.chapter_title(num, title)
        self.chapter_body(name,
                          local_company,
                          local_author,
                          local_antenna_type,
                          local_description,
                          local_frequency_start,
                          local_frequency_stop,
                          local_frequency_start_stop_unit,
                          local_frequency_step_int,
                          local_frequency_step_hz,
                          local_frequency_step_unit,
                          bandwidth,
                          power_level,
                          local_azimuth_start,
                          local_azimuth_stop,
                          local_azimuth_step,
                          local_elevation_start,
                          local_elevation_stop,
                          local_elevation_step)

    # generates second chapter
    def print_surface_plots(self, num, title):
        self.ln()
        d = 0
        if FPDF.get_y(
                self) > 151:  # check if plots have to start on next page or existing page in case of large description
            self.add_page()
            d = 1
        self.chapter_title(num, title)
        n = 0
        for filename in os.listdir('exports\surfaceplot_buffer'):  # loop through all folder and add png'glob_settings to pdf
            f = os.path.join('exports\surfaceplot_buffer', filename)
            # checking if it is a file
            if os.path.isfile(f):
                print("Adding to pdf:", f)
                if n == 0:  # check if surface plot is the first one printed in pdf, then different position apply due
                    # to chapter title
                    self.image(f, 10, FPDF.get_y(self) - d * 5, 135)
                elif (
                        n + d) % 2 == 1:  # check if surface plot has to be in first position, if so, add page and
                    # print image
                    self.add_page()
                    self.image(f, 10, 30, 135)
                elif (n + d) % 2 == 0:  # check if surface plot has to be in second position, if so, print image
                    self.image(f, 10, 160, 135)
                n = n + 1


# Function for external calling of module
def generate_pdf(gui,
                 title,
                 company,
                 author,
                 antenna_type,
                 description,
                 frequency_start,
                 frequency_stop,
                 frequency_start_stop_unit,
                 frequency_step_int,
                 frequency_step_hz,
                 frequency_step_unit,
                 bandwidth,
                 power_level,
                 azimuth_start,
                 azimuth_stop,
                 azimuth_step,
                 elevation_start,
                 elevation_stop,
                 elevation_step,
                 path = "exports"):
    global pdf_title
    pdf_title = title
    pdf = Pdf()
    pdf.alias_nb_pages()
    pdf.set_author(author)
    pdf.set_font('Times', '', 12)
    pdf.print_chapter(1, 'Measurement parameters:', 'back_end/model/pdf/chapter_parameters.txt',
                      company,
                      author,
                      antenna_type,
                      description,
                      frequency_start,
                      frequency_stop,
                      frequency_start_stop_unit,
                      frequency_step_int,
                      frequency_step_hz,
                      frequency_step_unit,
                      bandwidth,
                      power_level,
                      azimuth_start,
                      azimuth_stop,
                      azimuth_step,
                      elevation_start,
                      elevation_stop,
                      elevation_step)
    pdf.print_surface_plots(2,
                            'Surface plots{}|{}{}'.format(frequency_start, frequency_stop, frequency_start_stop_unit))
    pdf.output('{}\{}{}.pdf'.format(path, title, time.strftime("%Y%m%d-%H%M")), 'freq')
    print("PDF:", title, "has been successfully generatred!")
    gui.enable_buttons()
