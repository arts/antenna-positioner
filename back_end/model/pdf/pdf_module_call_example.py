import time
from pdf_module import generate_pdf

company = 'Astron'
author = 'Mike van der Elst'
title = 'Positioner_Report_' + time.strftime("%d-%m-%Y_%H%M")

azimuth_start = -100
azimuth_stop = 100
azimuth_step = 41
elevation_start = -89
elevation_stop = 89
elevation_step = 37
frequency_start = 1
frequency_stop = 2
frequency_start_stop_unit = "gHz"
frequency_step_int = 21
frequency_step_hz = 50
frequency_step_unit = "mHz"
antenna_type = "50 Ohm Monopole"
description = "The definition of a description is a statement that gives details about someone or something.\nAn " \
              "example of description is a story about the places visited on a family trip. "

if __name__ == "__main__":
    generate_pdf(title,
                 company,
                 author,
                 antenna_type,
                 description,
                 frequency_start,
                 frequency_stop,
                 frequency_start_stop_unit,
                 frequency_step_int,
                 frequency_step_hz,
                 frequency_step_unit,
                 azimuth_start,
                 azimuth_stop,
                 azimuth_step,
                 elevation_start,
                 elevation_stop,
                 elevation_step)
