import back_end.model.plot.plot as plot
from back_end.model.plot.hoofdpijn_dictionary import test_dictionary as td

# example data
list_az = [-100.0, -95.0, -90.0, -85.0, -80.0, -75.0, -70.0, -65.0, -60.0, -55.0, -50.0, -45.0, -40.0, -35.0, -30.0,
           -25.0, -20.0, -15.0, -10.0, -5.0, 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 55.0, 60.0,
           65.0, 70.0, 75.0, 80.0, 85.0, 90.0, 95.0, 100.0]
list_el = [-89.0, -85.0, -80.0, -75.0, -70.0, -65.0, -60.0, -55.0, -50.0, -45.0, -40.0, -35.0, -30.0, -25.0, -20.0,
           -15.0, -10.0, -5.0, 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0,
           75.0, 80.0, 85.0, 90.0]
list_ports = ['VNA1P2', 'VNA1P3', 'VNA1P4', 'VNA2P2', 'VNA2P3', 'VNA2P4']
list_frequencies = range(1000, 2050, 50)


# Add all data in one go
def add_example_data():
    for n in list_el:
        for m in list_az:
            for port in range(0, len(list_ports), 1):
                # loops through all frequencies in data given that even is frequency and uneven
                # is phase
                for freq in range(0, 2 * len(list_frequencies),
                                  2):
                    plot.Plot.add_measurement(plot.object_list[int(freq / 2 + port * len(list_frequencies))],
                                              list_az.index(m), list_el.index(n), td[(str(m), str(n))][port][
                                                  freq])
                    # add data to object[] given data key azimuth elevation
                    # port and frequency


# For testing gui with updates!
row = 0
list_td_lines = []
for r in td:
    list_td_lines.append(r)


# adds data from row
def gui_example_data():
    global row
    print("Adding Example Data From td row:", row)
    for port in range(0, len(list_ports), 1):
        for freq in range(0, 2 * len(list_frequencies),
                          2):  # loops through all frequencies in data given that even is frequency and uneven is phase
            plot.Plot.add_measurement(plot.object_list[int(freq / 2 + port * len(list_frequencies))],
                                      list_az.index(float(list_td_lines[row][0])),
                                      list_el.index(float(list_td_lines[row][1])),
                                      td[(list_td_lines[row][0], list_td_lines[row][1])][port][
                                          freq])  # add data to object[] given data key azimuth(from current row)
            # elevation(from current row) port and frequency
    row = row + 1  # update row for next call


def main():
    plot.init_plot_objects(list_az, list_el, list_ports, list_frequencies)
    add_example_data()

    # for n in plot.object_list: n.draw_png(filepath = r"C:\Users\mike van der Elst\Desktop\Hanze
    # Hogeschool\Electrotechniek Mechantornica\Minoren\Internet of Things\Project Astron\Plot\PNG" ) n.draw_plot(
    # display = True)

    # for n in range(len(td)):
    #    gui_example_data()
    #    plot.object_list[50].draw_plot(display = True)


if __name__ == "__main__":
    main()
