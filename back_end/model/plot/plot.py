import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# list of all object made
object_list = []


class Plot:
    def __init__(self, name, list_az, list_el):
        self.name = "PLOTDATA_" + name
        self.X, self.Y = np.meshgrid(list_az, list_el)
        self.Array_Mag = [['nan' for _ in range(len(list_az))] for _ in range(len(list_el))]
        print(self.name, len(self.X), "|", len(self.Y), "|",
              len(self.Array_Mag))  # note matplotlib only works if data is of equal length

    def __repr__(self):
        return str(self.Array_Mag)  # returns data for debugging

    # generates png of surface plot data in current object and outputs it to buffer file at filepath
    def draw_png(self, filepath=''):
        z = np.array(self.Array_Mag)
        fig = plt.figure(figsize=(10, 8), clear=True, num=1)
        ax = plt.axes(projection='3d')
        surf = ax.plot_surface(self.X, self.Y, z, cmap=cm.viridis, rstride=1, cstride=1, alpha=None, linewidth=0.5,
                               antialiased=True)
        ax.set_title(self.name + 'MHz')
        ax.set_xlabel("Azimuth")
        ax.set_ylabel("Elevation")
        ax.set_zlabel("Gain")
        fig.colorbar(surf, shrink=0.5, aspect=5)
        filename = filepath + '\Surface' + self.name + 'MHz'
        print("Saving PNG:", filename)
        fig.savefig(filename, transparent=True, bbox_inches='tight', dpi=300, edgecolor=None)
        #fig.clear()
        #plt.close(fig2)

    # returns plot data for surface plot in gui
    # note display = True is for testing purpose and plots data in external window
    def draw_plot(self, display=False):
        z = np.array(self.Array_Mag)
        if display:
            fig = plt.figure(figsize=(10, 8), num=1, clear=True)
            ax = plt.axes(projection='3d')
            ax.plot_surface(self.X, self.Y, z, cmap=cm.viridis, rstride=1, cstride=1, alpha=None, linewidth=0.5,
                            antialiased=True)
            ax.set_title(self.name)
            ax.set_xlabel("Azimuth")
            ax.set_ylabel("Elevation")
            plt.show(block=False)
            plt.pause(0.1)
            fig.clear()
            plt.close(fig)
        else:
            return self.X, self.Y, z, self.name

    def add_measurement(self, index_az, index_el, data):
        self.Array_Mag[index_el][index_az] = data


def init_plot_objects(list_az, list_el, list_ports, list_frequencies):
    # clear list in case of memory leak when calling function more than ones
    object_list.clear()
    print("Amount of Plot objects is:", len(object_list))
    # generate object names with elements port and frequencies
    for n in list_ports:
        for m in list_frequencies:
            object_list.append(str(n) + "_" + str(m))
    # replace string in object_list with objects
    o = 0
    for n in object_list:
        object_list[o] = Plot(n, list_az, list_el)
        o = o + 1
