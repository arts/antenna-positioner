import time

Test_dictionary = {}

filename = "Lazy_Dictionary" + time.strftime("%Y%m%d-%H%M") + ".txt"
f = open(filename, "a")


def main():
    data_txt = open("meting_horn_hor_el_1_2_3_4_5_6.txt")
    buffer = data_txt.read()
    line_list = buffer.split("\n")  # make array for each line in file
    print("Test_dictionary = {", end="\n", file=f)  # make a Test 2d_dictionary with a 2d_array as output
    for n in range(3, len(line_list) - 1):  # loop though al the lines with data
        dictionary_az_key = line_list[n][0:line_list[n].find(",")]  # get Azimuth for dictionary
        dictionary_el_key = line_list[n][line_list[n].find(",") + 2:line_list[n].find(",", line_list[n].find(
            ",") + 1)]  # get Elevation for dictionary
        # print(dictionary_az_key + " , " + dictionary_el_key)
        data = line_list[n][line_list[n].find("["):len(line_list[n])]  # get array containing data
        # Test_dictionary[(str(dictionary_el_key),str(dictionary_az_key))] = data
        print("('{}', '{}'): [".format(dictionary_az_key, dictionary_el_key) + data + "],", end="\n",
              file=f)  # add data to dictionary
        # time.sleep(1)
    print('}', file=f)
    f.close()
    print(Test_dictionary)


if __name__ == "__main__":
    main()
