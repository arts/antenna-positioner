from setuptools import setup

setup(
    name='antenna-positioner',
    version='',
    packages=['back_end',
              'back_end.model',
              'back_end.model.pdf',
              'back_end.model.config',
              'back_end.model.plot',
              'front_end'],
    url='',
    license='',
    author='rickr',
    author_email='',
    description='',
    install_requires=[
        'fpdf2',
        'numpy',
        'matplotlib',
        'RSinstrument',
        'scipy'
    ]
)
