# Antenna positioner

This project contains the Python code developed by students from Hanzehogeschool for controlling the positioner in the antenna measurement room.


# Function explanation

Front-end functions:

Functions of Antenna_positioner_gui.py:

The enable_buttons() & disable_buttons() functions unlock and lock the main page menu buttons during operation of the test facility so settings cannot be tampered when running tests. 
 
The start_measurement() function calls measurement functions through the controller and starts the plotting of received measurement data on the surface plot.
 
The switch_to_page() function saves entered data first from the current page, then clears the entire window in order to switch to widgets of other pages again
 
surface_plot_gui() is the surface plot generation function. This uses Matplotlib to create the visualisation of measurement data.
The  main_page(), configuration_page(), and measurement_settings_page()are the functions containing all code dedicated to page-specific options and widgets. 

The init_gui_elements()function contains initialisation of UI elements that are consistent across most pages on the UI.
 
The  main()function calls main_page()when starting the application for the first time and the window.mainloop() function of Tkinter which loads the GUI.
 

## Usage
If the program has never been run before, the setup.bat file should be run once, to install all the required python packages.

Once all the required packages have been installed, the program can be run using the start.bat file.
